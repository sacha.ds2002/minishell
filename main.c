/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/29 13:35:23 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/03 16:07:56 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	main(int argc, char **argv, char **env)
{
	char	**paths;
	t_token	*token;
	t_env	*env_lst;

	(void) argc;
	(void) argv;
	env_lst = ft_convert_env(env);
	ft_printf("\n\n\033[0;32m--- Minishell ---\033[0;37m\n\n");
	g_status = 0;
	while (1)
	{
		signal_init(prompt_handler);
		paths = ft_get_paths(ft_get_env_var("PATH", 4, env_lst));
		token = parsing(init_line(), env_lst);
		execution(token, paths, &env_lst);
		ft_clear_tokens(&token);
		free_char_array(&paths);
	}
	return (0);
}
