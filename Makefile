# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/08/29 15:44:39 by sada-sil          #+#    #+#              #
#    Updated: 2023/11/06 15:21:15 by sada-sil         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = minishell

RED = \033[0;31m
GREEN = \033[0;32m
BLUE = \033[0;34m
PURPLE = \033[0;35m
WHITE = \033[0;37m

CC = cc
# -fsanitize=address
CFLAGS = -Wall -Werror -Wextra  -I$(HOME)/.brew/opt/readline/include
RM = rm -f

LIBFT_PATH = libft/libft.a

READLINE =  -lreadline -L$(HOME)/.brew/opt/readline/lib

SRCS = main.c \
		src/others/line_init.c \
		src/others/utils.c \
		src/others/signal_handler.c \
		src/parsing/parsing.c \
		src/parsing/ft_get_tokens.c \
		src/parsing/ft_split_quotes.c \
		src/parsing/token_lst_utils.c \
		src/parsing/token_utils.c \
		src/parsing/param_utils.c \
		src/parsing/word_utils.c \
		src/parsing/env_variables.c \
		src/parsing/env_var_utils.c \
		src/parsing/heredoc.c \
		src/builtins/cd.c \
		src/builtins/echo.c \
		src/builtins/env.c \
		src/builtins/exit.c \
		src/builtins/export.c \
		src/builtins/pwd.c \
		src/builtins/unset.c \
		src/execution/execution.c \
		src/execution/child_process.c \
		src/execution/redirections.c \
		src/execution/env_utils.c \
		src/execution/env_utils2.c \
		src/execution/exec_utils.c

OBJS = ${SRCS:.c=.o}

${NAME}:	Clibft	CFiles	${OBJS}
	@echo "${BLUE}Files compiled.${WHITE}"
	@echo "${RED}Compile Minishell.${WHITE}"
	@${CC} ${CFLAGS} ${READLINE} ${OBJS} -o ${NAME} ${LIBFT_PATH}
	@echo "${RED}Minishell compiled.${WHITE}"

Clibft:
	@echo "${GREEN}Compile Libft.${WHITE}"
	@make bonus -C libft
	@echo "${GREEN}Libft compiled.${WHITE}"

CFiles:
	@echo "${BLUE}Compile Files.${WHITE}"

all:	${NAME}

clean:
	@make fclean -C libft
	@echo "${PURPLE}Removed libft files.${WHITE}"
	@(${RM} ${OBJS})
	@echo "${PURPLE}Removed compiled files.${WHITE}"

fclean: clean
	@(${RM} ${NAME} ${LIBFT_PATH})
	@echo "${PURPLE}Removed executable.${WHITE}"

re: fclean all

.PHONY: all clean fclean re
.SILENT: $(OBJS)