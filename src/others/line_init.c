/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/31 09:56:25 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/02 15:16:53 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

char	*init_line(void)
{
	char	*line;

	line = readline(GREEN"minishell> "RESET_COLOR);
	if (line == NULL)
		exit(0);
	signal_init(exec_handler);
	return (line);
}
