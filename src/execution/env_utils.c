/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/31 14:33:25 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/03 17:01:47 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

static t_env	*ft_get_last_env(t_env *lst)
{
	while (lst && lst->next != NULL)
		lst = lst->next;
	return (lst);
}

t_env	*ft_create_env_var(char	*str)
{
	t_env	*new;

	new = (t_env *)malloc(sizeof(t_env));
	if (!new)
		return (NULL);
	new->next = NULL;
	new->str = ft_strdup(str);
	return (new);
}

void	ft_env_addback(t_env **lst, t_env *new)
{
	if (!new)
		return ;
	if (*lst == NULL)
		*lst = new;
	else
		(ft_get_last_env(*lst))->next = new;
}

char	**ft_env_to_string(t_env *env)
{
	char	**ret;
	int		count;
	int		i;

	count = ft_count_env_var(env) + 1;
	ret = (char **)ft_calloc(sizeof(char *), count);
	i = -1;
	while (env && env->str)
	{
		ret[++i] = ft_strdup(env->str);
		if (env->next == NULL)
			break ;
		env = env->next;
	}
	return (ret);
}

t_env	*ft_convert_env(char **env)
{
	t_env	*ret;
	t_env	*new;
	int		i;

	if (!env || !env[0])
		return (NULL);
	ret = NULL;
	i = -1;
	while (env[++i])
	{
		new = (t_env *)malloc(sizeof(t_env));
		new->str = ft_strdup(env[i]);
		new->next = NULL;
		ft_env_addback(&ret, new);
		new = NULL;
	}
	return (ret);
}
