/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   child_process.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/25 11:41:37 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/06 14:25:21 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

static int	ft_count_params(t_param *param)
{
	int	count;

	count = 0;
	while (param != NULL)
	{
		count++;
		if (param->next == NULL)
			break ;
		param = param->next;
	}
	return (count);
}

static char	**ft_get_params(t_param *param)
{
	char	**ret;
	t_param	*tmp;
	int		count;
	int		i;

	if (param == NULL)
		return ((char **)ft_calloc(sizeof(char *), 2));
	count = ft_count_params(param) + 1;
	tmp = param;
	ret = (char **)ft_calloc(sizeof(char *), (count + 1));
	i = 1;
	while (tmp != NULL)
	{
		ret[i] = ft_strdup(tmp->str);
		if (tmp->next == NULL)
			break ;
		tmp = tmp->next;
		i++;
	}
	return (ret);
}

static void	try_paths(t_token *token, char **params, char **paths, t_env *env)
{
	char	*cmd;
	int		i;

	i = -1;
	while (++i < ((int) sizeof(paths)) - 1)
	{
		cmd = ft_strjoin(paths[i], token->file);
		params[0] = ft_strdup(cmd);
		if (access(cmd, X_OK) == 0)
			if (execve(cmd, params, ft_env_to_string(env)) == -1)
				return (perror("execve"));
		free(cmd);
		free(params[0]);
	}
}

void	ft_child_process(t_token *token, char **paths, t_env *env)
{
	char			**params;
	unsigned long	i;

	params = ft_get_params(token->param);
	params[0] = ft_strdup(token->file);
	i = -1;
	if (access(token->file, X_OK) == 0)
		if (execve(token->file, params, ft_env_to_string(env)) == -1)
			perror("execve");
	free(params[0]);
	if (paths != NULL && paths[0] != NULL)
		try_paths(token, params, paths, env);
	perror(ft_strjoin("minishell: ", token->file));
	exit(127);
}
