/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirections.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/25 13:21:28 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/03 17:08:25 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

static void	redir_output(t_token *token)
{
	int	fd;

	if (token->append == 0)
		fd = open(token->output, O_WRONLY | O_CREAT | O_TRUNC, 0644);
	else
		fd = open(token->output, O_WRONLY | O_CREAT | O_APPEND, 0644);
	if (fd == -1)
		exit_on_failure("open");
	dup2(fd, STDOUT_FILENO);
	close(fd);
}

static void	direct_redirections(t_token *token)
{
	int	fd;

	if (ft_strcmp(token->output, DEFAULT_OUTPUT) != 0)
		redir_output(token);
	if (token->delim != NULL)
	{
		write(token->end[1], token->input, ft_strlen(token->input));
		dup2(token->end[0], STDIN_FILENO);
	}
	else if (ft_strcmp(token->input, DEFAULT_INPUT) != 0)
	{
		fd = open(token->input, O_RDONLY);
		if (fd == -1)
			exit_on_failure("open");
		dup2(fd, STDIN_FILENO);
		close(fd);
	}
}

static void	last_token(char *delim, int p1[2], int default_stdout)
{
	if (!delim)
		dup2(p1[0], STDIN_FILENO);
	dup2(default_stdout, STDOUT_FILENO);
}

static void	middle_token(char *delim, int p1[2], int p2[2])
{
	if (!delim)
		dup2(p1[0], STDIN_FILENO);
	dup2(p2[1], STDOUT_FILENO);
}

void	redirections(t_token *token, int default_stdout)
{	
	if (token->id != 0 || token->next != NULL)
	{
		if (token->id == 0)
			dup2(((t_token *)token->next)->end[1], STDOUT_FILENO);
		else if (token->next != NULL)
			middle_token(token->delim, token->end,
				((t_token *)token->next)->end);
		else
			last_token(token->delim, token->end, default_stdout);
	}
	direct_redirections(token);
	close_pipes(token->end);
}
