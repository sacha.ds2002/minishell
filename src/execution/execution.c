/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execution.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/30 15:31:56 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/06 14:23:48 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

static void	wait_children(pid_t *pids)
{
	int	i;
	int	status;

	i = -1;
	status = 0;
	if (pids == NULL)
		return ;
	while (pids[++i] != LAST_PID)
		if (pids[i] != 0)
			waitpid(pids[i], &status, 0);
	if (WIFEXITED(status))
		g_status = WEXITSTATUS(status);
}

static int	ft_exec_builtins(t_token *token, t_env **env)
{
	if (ft_strcmp(token->file, "cd") == 0)
		g_status = ft_cd(token->param, *env);
	else if (ft_strcmp(token->file, "echo") == 0)
		g_status = ft_echo(token->param);
	else if (ft_strcmp(token->file, "env") == 0)
		g_status = ft_env(*env);
	else if (ft_strcmp(token->file, "export") == 0)
		g_status = ft_export(token->param, env);
	else if (ft_strcmp(token->file, "pwd") == 0)
		g_status = ft_pwd();
	else if (ft_strcmp(token->file, "unset") == 0)
		g_status = ft_unset(token->param, env);
	else if (ft_strcmp(token->file, "exit") == 0)
		ft_exit(token->param);
	else
		g_status = -1;
	return (g_status);
}

static pid_t	exec_with_fork(t_token *token, t_env **env, char **paths)
{
	pid_t	child_pid;

	child_pid = fork();
	if (child_pid == -1)
		exit_on_failure("fork");
	if (child_pid == 0)
	{
		redirections(token, dup(STDOUT_FILENO));
		if (ft_is_builtin(token->file) != 0)
			exit(ft_exec_builtins(token, env));
		else
			ft_child_process(token, paths, *env);
	}
	else
		close_pipes(token->end);
	return (child_pid);
}

static pid_t	exec_solo_cmd(t_token *token, t_env **env, char **paths)
{
	if (ft_is_builtin(token->file) != 0)
		ft_exec_builtins(token, env);
	else
		return (exec_with_fork(token, env, paths));
	return (0);
}

void	execution(t_token *token, char **paths, t_env **env)
{
	pid_t	*pids;

	pids = (pid_t *)malloc(sizeof(pid_t) * (ft_lstlast_token(token)->id + 2));
	if (pids == NULL)
		exit_on_failure("Memory alloc pid");
	pids[ft_lstlast_token(token)->id + 1] = LAST_PID;
	if (token && token->file != NULL
		&& token->next == NULL && token->id == 0)
		pids[token->id] = exec_solo_cmd(token, env, paths);
	else
	{
		while (token && token->file != NULL)
		{
			pids[token->id] = exec_with_fork(token, env, paths);
			if (token->next == NULL)
				break ;
			token = token->next;
		}
	}
	wait_children(pids);
	free(pids);
}
