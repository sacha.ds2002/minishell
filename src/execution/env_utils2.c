/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_utils2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/01 12:08:26 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/03 17:03:51 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

int	ft_count_env_var(t_env *env)
{
	int	i;

	i = 0;
	while (env)
	{
		i++;
		if (env->next == NULL)
			break ;
		env = env->next;
	}
	return (i);
}

char	**ft_get_paths(char *env)
{
	char	*tmp;
	char	**ret;
	int		i;

	if (!env)
		return (NULL);
	tmp = ft_substr(env, 5, ft_strlen(env) - 5);
	ret = ft_split(tmp, ':');
	free(tmp);
	i = -1;
	while (ret[++i] != NULL)
	{
		tmp = ft_strdup(ret[i]);
		free(ret[i]);
		ret[i] = ft_strjoin(tmp, "/");
		free(tmp);
	}
	free(env);
	return (ret);
}

char	*ft_get_env_var(char *name, int len, t_env *env)
{
	while (env)
	{
		if (ft_strncmp(name, env->str, len) == 0 && env->str[len] == '=')
		{
			return (ft_strdup(env->str + len + 1));
		}
		if (env->next == NULL)
			break ;
		env = env->next;
	}
	return (ft_strdup(" "));
}

int	ft_is_builtin(char *file)
{
	return (ft_strcmp(file, "cd") == 0
		|| ft_strcmp(file, "echo") == 0
		|| ft_strcmp(file, "env") == 0
		|| ft_strcmp(file, "export") == 0
		|| ft_strcmp(file, "pwd") == 0
		|| ft_strcmp(file, "unset") == 0
		|| ft_strcmp(file, "exit") == 0);
}
