/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/30 14:07:01 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/06 14:03:00 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

t_token	*parsing(char *ol, t_env *env)
{
	t_word	*word;
	t_token	*token;

	word = ft_split_quotes(ol, ' ');
	if (ol != NULL && word != NULL)
		add_history(ol);
	free(ol);
	ft_get_envariables(word, env);
	token = ft_get_tokens(word);
	ft_clear_words(&word);
	return (token);
}
