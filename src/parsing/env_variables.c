/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_variables.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/14 14:20:35 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/03 16:53:37 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

static char	*ft_replace_var(char *str, char *content)
{
	int		len;
	int		len_after;
	char	*ret;
	char	*tmp;
	char	*tmp2;

	if (!content)
		content = ft_strdup("");
	len = ft_tot_len(str, ft_strlen(content));
	tmp2 = ft_strndup(str, ft_strlen_c(str, '$'));
	tmp = ft_strjoin(tmp2, content);
	free(tmp2);
	len_after = ft_len_after(str);
	tmp2 = ft_substr(str, ft_strlen(str) - len_after, len_after);
	ret = ft_strjoin(tmp, tmp2);
	free(str);
	free(tmp);
	free(tmp2);
	return (ret);
}

static char	*ft_get_var_content(char *str, t_env *env)
{
	int		len;
	char	*tmp;
	char	*ret;

	len = ft_len_var(str);
	tmp = ft_strndup(str, len);
	ret = ft_get_env_var(tmp, len, env);
	free(tmp);
	return (ret);
}

static char	*ft_search_dollar(char *str, t_env *env)
{
	char	*tmp;
	int		i;

	i = -1;
	while (str[++i])
	{
		if (str[i] == '$')
		{
			i++;
			if (str[i] && str[i] != '\0')
			{
				if (str[i] == '?')
					tmp = ft_itoa(g_status);
				else
					tmp = ft_get_var_content(&(str[i]), env);
				str = ft_replace_var(str, tmp);
				free(tmp);
				i = -1;
			}
			else
				break ;
		}
	}
	return (str);
}

void	ft_get_envariables(t_word *word, t_env *env)
{
	while (word)
	{
		if (word->squote == 1)
			word->str = ft_search_dollar(word->str, env);
		word = word->next;
	}
}
