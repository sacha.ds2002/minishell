/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   word_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/14 10:15:13 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/06 14:14:21 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

void	ft_clear_words(t_word **word)
{
	t_word	*tmp_word;

	while (*word)
	{
		tmp_word = (*word)->next;
		if ((*word)->str)
			free((*word)->str);
		free(*word);
		if (!tmp_word)
			break ;
		*word = tmp_word;
	}
}

t_word	*ft_create_word(char const *str, int length, int sep)
{
	t_word	*word;

	word = (t_word *)malloc(sizeof(t_word));
	if (!word)
		return (NULL);
	word->next = NULL;
	if (sep == '\'')
		word->squote = 0;
	else
		word->squote = 1;
	word->str = ft_strndup(str, length);
	if (!word->str)
	{
		free(word);
		return (NULL);
	}
	return (word);
}

t_word	*ft_lstlast_word(t_word *lst)
{
	while (lst && lst->next != NULL)
		lst = lst->next;
	return (lst);
}

void	ft_word_addback(t_word **lst, t_word *new)
{
	if (!new)
		return ;
	if (*lst == NULL)
		*lst = new;
	else
		(ft_lstlast_word(*lst))->next = new;
}
