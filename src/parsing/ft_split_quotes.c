/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_quotes.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/08 13:57:31 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/03 16:54:32 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

static char	*ft_skip_chars(char *s, char c, int *param_sep)
{
	while (*s && (*s == c || *s == '\'' || *s == '\"') && *param_sep == c)
	{
		if (*s == '\'' || *s == '\"')
			*param_sep = (int)(*s);
		s++;
	}
	return (s);
}

static int	ft_count_strs(char *s, int c)
{
	int		nb;
	int		param_sep;

	nb = 0;
	param_sep = c;
	s = ft_skip_chars(s, c, &param_sep);
	while (*s)
	{
		nb++;
		if ((char)param_sep != ' ')
			while (*s && *s != param_sep)
				s++;
		else
			while (*s && *s != ' ' && *s != '\'' && *s != '\"')
				s++;
		if (*s != '\0' && *s && *s == (char)param_sep)
			s++;
		param_sep = c;
		s = ft_skip_chars(s, c, &param_sep);
	}
	return (nb);
}

int	ft_exec_split(t_word **word, char *s, int param_sep)
{
	int	word_len;

	word_len = 0;
	if (param_sep == ' ')
	{
		while (s[word_len] && s[word_len] != ' '
			&& s[word_len] != '\'' && s[word_len] != '\"')
			word_len++;
	}
	else
		word_len = ft_strlen_c(s, param_sep);
	ft_word_addback(word,
		ft_create_word(s, word_len, param_sep));
	if ((char)param_sep != ' ')
		word_len++;
	return (word_len);
}

t_word	*ft_split_quotes(char *s, char c)
{
	t_word	*result;
	int		count;
	int		i;
	int		param_sep;

	count = ft_count_strs(s, c);
	param_sep = 0;
	result = NULL;
	i = 0;
	while (i < count)
	{
		param_sep = (int)c;
		s = ft_skip_chars(s, c, &param_sep);
		s += ft_exec_split(&result, s, param_sep);
		i++;
	}
	return (result);
}
