/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   param_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 09:09:02 by sada-sil          #+#    #+#             */
/*   Updated: 2023/10/19 15:37:13 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

void	ft_clear_params(t_param **param)
{
	t_param	*tmp_param;

	while (*param)
	{
		tmp_param = (*param)->next;
		if ((*param)->str)
			free((*param)->str);
		free(*param);
		if (!tmp_param)
			break ;
		*param = tmp_param;
	}
}

t_param	*ft_create_param(char *str)
{
	t_param	*param;

	param = (t_param *)malloc(sizeof(t_param));
	if (!param)
		return (NULL);
	param->next = NULL;
	param->str = ft_strdup(str);
	if (!param->str)
	{
		free(param);
		return (NULL);
	}
	return (param);
}

t_param	*ft_lstlast_param(t_param *lst)
{
	while (lst && lst->next != NULL)
		lst = lst->next;
	return (lst);
}

void	ft_param_addback(t_param **lst, t_param *new)
{
	if (!new)
		return ;
	if (*lst == NULL)
		*lst = new;
	else
		(ft_lstlast_param(*lst))->next = new;
}
