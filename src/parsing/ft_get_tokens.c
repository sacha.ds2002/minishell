/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_tokens.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/11 13:28:01 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/03 16:58:49 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

static t_token	*ft_create_token(int id)
{
	t_token	*token;

	token = (t_token *)malloc(sizeof(t_token));
	if (!token)
		return (NULL);
	token->id = id;
	token->input = ft_strdup(DEFAULT_INPUT);
	token->output = ft_strdup(DEFAULT_OUTPUT);
	if (pipe(token->end) == -1)
		exit_on_failure("pipe");
	token->file = NULL;
	token->param = NULL;
	token->delim = NULL;
	token->append = 0;
	token->next = NULL;
	return (token);
}

static t_word	*ft_fill_token(t_token *token, t_word *word)
{
	int	param;
	int	type;

	param = 0;
	while (word != NULL)
	{
		type = ft_get_type(word->str);
		if (type == 0)
		{
			if (param == 0)
				token->file = ft_strdup(word->str);
			else
				ft_param_addback(&(token->param),
					ft_create_param(word->str));
			param++;
		}
		if (type >= 1 && type <= 4)
			word = ft_set_input_output(&token, word, type);
		word = word->next;
		if (type == 5)
			break ;
	}
	return (word);
}

static int	fill_token_list(t_word *word, t_token **token)
{
	int		id;
	t_token	*tmp_token;

	id = 0;
	while (word != NULL)
	{
		if (id == 0)
		{
			word = ft_fill_token(*token, word);
			id++;
		}
		else
		{
			tmp_token = ft_create_token(id);
			if (!tmp_token)
			{
				ft_clear_tokens(token);
				return (1);
			}
			word = ft_fill_token(tmp_token, word);
			ft_token_addback(token, tmp_token);
			id++;
		}
	}
	return (0);
}

t_token	*ft_get_tokens(t_word *word)
{
	int		id;
	t_token	*token;

	id = 0;
	token = ft_create_token(id);
	if (!token)
		return (NULL);
	if (fill_token_list(word, &token) != 0)
		return (NULL);
	return (token);
}
