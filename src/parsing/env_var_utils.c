/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_var_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/14 15:32:39 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/01 12:11:07 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

int	ft_len_var(char *str)
{
	int	len;

	len = ft_strlen_c(str, ' ');
	if (len > ft_strlen_c(str, '$'))
		len = ft_strlen_c(str, '$');
	if (len > ft_strlen_c(str, '\''))
		len = ft_strlen_c(str, '\'');
	if (len > ft_strlen_c(str, '\"'))
		len = ft_strlen_c(str, '\"');
	return (len);
}

int	ft_len_after(char *str)
{
	str += ft_strlen_c(str, '$') + 1;
	str += ft_len_var(str);
	return (ft_strlen(str));
}

int	ft_tot_len(char *str, int var_len)
{
	int	len_before;
	int	len_after;

	len_before = ft_strlen_c(str, '$');
	str += len_before;
	while (str && *str != ' ' && *str != '$')
		str++;
	len_after = ft_strlen(str);
	return (len_before + var_len + len_after);
}

int	ft_strlen_c(char *str, char c)
{
	int	i;

	i = 0;
	while (str[i] && str[i] != c)
		i++;
	return (i);
}
