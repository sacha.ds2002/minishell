/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/19 14:14:03 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/06 15:23:14 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

static char	*ft_strjoin_f(char **s1, char *s2)
{
	char	*str;
	int		len;
	int		i;
	int		j;

	i = -1;
	j = -1;
	len = ft_strlen(*s1) + ft_strlen(s2);
	str = (char *)ft_calloc(sizeof(char), (len + 1));
	if (!str)
		return (0);
	while ((*s1)[++i])
		str[i] = (*s1)[i];
	while (s2[++j])
		str[i++] = s2[j];
	str[i] = '\0';
	free(*s1);
	return (str);
}

char	*ft_create_heredoc(char	*delim)
{
	char	*tmp;
	char	*ret;

	ret = NULL;
	tmp = NULL;
	while (1)
	{
		tmp = readline("> ");
		if (tmp)
		{
			if (ft_strncmp(delim, tmp, ft_strlen(delim)) == 0)
				break ;
			tmp = ft_strjoin_f(&tmp, "\n");
			if (ret != NULL)
				ret = ft_strjoin_f(&ret, tmp);
			else
				ret = ft_strdup(tmp);
			free(tmp);
		}
	}
	if (tmp)
		free(tmp);
	return (ret);
}
