/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/12 12:32:13 by sada-sil          #+#    #+#             */
/*   Updated: 2023/10/27 10:32:22 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

static t_word	*ft_trim_word(t_word *word, int type)
{
	if ((type == 1 || type == 2) && ft_strlen(word->str) > 1)
	{
		if (type == 1)
			word->str = ft_strtrim(word->str, "<");
		else
			word->str = ft_strtrim(word->str, ">");
	}
	else if (ft_strlen(word->str) > 2)
	{
		if (type == 3)
			word->str = ft_strtrim(word->str, "<");
		else
			word->str = ft_strtrim(word->str, ">");
	}
	else if (word->next != NULL)
		word = word->next;
	return (word);
}

void	ft_clear_tokens(t_token **token)
{
	t_token	*tmp_token;

	while (*token)
	{
		tmp_token = (*token)->next;
		ft_clear_params(&((*token)->param));
		free((*token)->input);
		free((*token)->output);
		if ((*token)->file)
			free((*token)->file);
		if ((*token)->delim)
			free((*token)->delim);
		close_pipes((*token)->end);
		free(*token);
		*token = tmp_token;
	}
}

t_word	*ft_set_input_output(t_token **token, t_word *word, int type)
{
	word = ft_trim_word(word, type);
	if (type == 1)
	{
		free((*token)->input);
		(*token)->input = ft_strdup(word->str);
	}
	else if (type == 3)
	{
		(*token)->delim = ft_strdup(word->str);
		free((*token)->input);
		(*token)->input = ft_create_heredoc((*token)->delim);
	}
	else if (type == 2 || type == 4)
	{
		free((*token)->output);
		(*token)->output = ft_strdup(word->str);
		if (type == 4)
			(*token)->append = 1;
	}
	return (word);
}

int	ft_get_type(char *str)
{
	int	type;

	type = 0;
	if (ft_strncmp(str, "<<", 2) == 0)
		type = 3;
	else if (ft_strncmp(str, ">>", 2) == 0)
		type = 4;
	else if (*str == '<')
		type = 1;
	else if (*str == '>')
		type = 2;
	else if (*str == '|')
		type = 5;
	return (type);
}
