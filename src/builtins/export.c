/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   export.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/11 12:53:29 by xeherzi           #+#    #+#             */
/*   Updated: 2023/11/03 16:18:41 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

static void	ft_print_env(char **env)
{
	int		i;
	char	*name;

	i = -1;
	while (env[++i])
	{
		name = ft_strndup(env[i], ft_strlen_c(env[i], '=') + 1);
		ft_printf("declare -x %s\"%s\"\n", name,
			&(env[i][ft_strlen_c(env[i], '=') + 1]));
		free(name);
	}
}

static void	bubble_sort(char ***env_dup, int count)
{
	char	*tmp;
	int		i;

	while (count > 0)
	{
		i = 0;
		while (i < count)
		{
			if ((*env_dup)[i + 1] == NULL)
				break ;
			if (ft_strcmp((*env_dup)[i], (*env_dup)[i + 1]) > 0)
			{
				tmp = ft_strdup((*env_dup)[i]);
				free((*env_dup)[i]);
				(*env_dup)[i] = ft_strdup((*env_dup)[i + 1]);
				free((*env_dup)[i + 1]);
				(*env_dup)[i + 1] = ft_strdup(tmp);
				free(tmp);
			}
			i++;
		}
		count--;
	}
}

static void	ft_sort_env(t_env *env)
{
	char	**env_dup;
	int		count;

	count = ft_count_env_var(env);
	env_dup = ft_env_to_string(env);
	bubble_sort(&env_dup, count);
	ft_print_env(env_dup);
	free_char_array(&env_dup);
}

static int	ft_replace(t_env **env, char *name, char *str)
{
	t_env	*tmp;

	tmp = *env;
	while (tmp)
	{
		if (ft_strncmp(tmp->str, name, ft_strlen(name)) == 0
			&& tmp->str[ft_strlen(name)] == '=')
		{
			free(tmp->str);
			tmp->str = ft_strdup(str);
			free(name);
			return (0);
		}
		if (tmp->next == NULL)
			break ;
		tmp = tmp->next;
	}
	return (1);
}

int	ft_export(t_param *param, t_env **env)
{
	int		i;
	char	*name;

	i = -1;
	if (param && param->str)
	{
		name = ft_strndup(param->str, ft_strlen_c(param->str, '='));
		if (name == NULL)
			return (-1);
		if (ft_replace(env, name, param->str) == 0)
			return (0);
		ft_env_addback(env, ft_create_env_var(param->str));
		free(name);
	}
	else
		ft_sort_env(*env);
	return (0);
}
