/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unset.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/11 12:53:23 by xeherzi           #+#    #+#             */
/*   Updated: 2023/11/03 16:48:31 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

static int	ft_unset_var(t_env **env, t_param *param, int len)
{
	t_env	*last;
	t_env	*tmp;

	tmp = *env;
	last = NULL;
	while (tmp)
	{
		if (ft_strncmp(param->str, tmp->str, len) == 0
			&& tmp->str[len] == '=')
		{
			if (last == NULL && (*env)->next != NULL)
				*env = (*env)->next;
			else
				last->next = tmp->next;
			free(tmp->str);
			free(tmp);
			return (0);
		}
		last = tmp;
		if (tmp->next == NULL)
			break ;
		tmp = tmp->next;
	}
	return (1);
}

int	ft_unset(t_param *param, t_env **env)
{
	int	len;

	if (param && param->str)
	{
		len = ft_strlen(param->str);
		if (ft_unset_var(env, param, len) == 0)
			return (0);
	}
	write(STDERR_FILENO, "minishell: unset: not a valid identifier\n", 41);
	return (-1);
}
