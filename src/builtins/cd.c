/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/11 12:53:08 by xeherzi           #+#    #+#             */
/*   Updated: 2023/11/06 14:39:54 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

static int	ft_free_error(char **curdir, char **path, char *str)
{
	if (*curdir != NULL)
		free(*curdir);
	if (*path != NULL)
		free(*path);
	if (str != NULL)
		perror(str);
	return (1);
}

static char	*ft_manage_home(t_param *param, t_env *env)
{
	char	*home_dir;
	char	*ret;

	ret = NULL;
	home_dir = ft_get_env_var("HOME", 4, env);
	if (home_dir == NULL)
		return (NULL);
	if (ft_strlen(param->str) == 1
		|| param->str[1] != '/' || param->str[1] == '\0')
		ret = ft_strdup(home_dir);
	else if (ft_strlen(param->str) > 1)
		ret = ft_strjoin(home_dir, param->str + 1);
	free(home_dir);
	return (ret);
}

int	ft_param_len(t_param *param)
{
	int	i;

	if (param == NULL || param->str == NULL)
		return (0);
	i = 0;
	while (param->str[i])
		i++;
	return (i);
}

static char	*ft_parse_arg(t_param *param, t_env *env)
{
	char	*ret;

	ret = NULL;
	if (param == NULL || param->str == NULL)
		ret = ft_get_env_var("HOME", 4, env);
	else if (param != NULL && ft_strncmp(param->str, "~", 1) == 0)
	{
		ret = ft_manage_home(param, env);
		if (ret == NULL)
		{
			ft_putstr_fd("Syntax not supported: ", STDERR_FILENO);
			ft_putstr_fd(param->str, STDERR_FILENO);
			ft_putstr_fd(".\n", STDERR_FILENO);
		}
	}
	else if (param != NULL)
	{
		ret = ft_strdup(param->str);
	}
	return (ret);
}

int	ft_cd(t_param *param, t_env *env)
{
	char	*path;
	char	*curdir;

	curdir = getcwd(NULL, 0);
	path = ft_parse_arg(param, env);
	if (path == NULL)
		return (ft_free_error(&curdir, NULL, "Error while parsing new path"));
	if (chdir(path) != 0)
	{
		perror("minishell: chdir");
		return (ft_free_error(&curdir, &path, NULL));
	}
	ft_free_error(&curdir, &path, NULL);
	return (0);
}
