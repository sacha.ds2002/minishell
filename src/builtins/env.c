/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/11 12:53:14 by xeherzi           #+#    #+#             */
/*   Updated: 2023/11/06 14:40:49 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

int	ft_env(t_env *env)
{
	if (!env || !(env->str))
	{
		ft_putstr_fd("Environment variables array is NULL", STDERR_FILENO);
		return (0);
	}
	while (env)
	{
		ft_printf("%s\n", env->str);
		if (env->next == NULL)
			break ;
		env = env->next;
	}
	return (1);
}		
