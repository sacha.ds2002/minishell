/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/11 12:53:11 by xeherzi           #+#    #+#             */
/*   Updated: 2023/11/03 16:11:00 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../minishell.h"

static int	count_arguments(t_param *param)
{
	int	size;

	size = 0;
	while (param != NULL)
	{
		size++;
		if (param->next == NULL)
			break ;
		param = param->next;
	}
	return (size);
}

int	ft_process_n(t_param **param)
{
	int		n_option;

	n_option = 0;
	while (*param != NULL && ft_strcmp((*param)->str, "-n") == 0)
	{
		n_option = 1;
		if ((*param)->next == NULL)
			break ;
		*param = (*param)->next;
	}
	if (*param != NULL
		&& ft_strcmp((*param)->str, "-n") == 0)
		*param = (*param)->next;
	return (n_option);
}

int	ft_echo(t_param *param)
{
	int		n_option;

	if (count_arguments(param) >= 1)
	{
		n_option = ft_process_n(&param);
		while (param != NULL)
		{
			if (param->str)
				ft_printf("%s", param->str);
			if (param->next != NULL)
				write(1, " ", 1);
			else
				break ;
			param = param->next;
		}
	}
	else
		return (1);
	if (n_option == 0)
		write(1, "\n", 1);
	return (0);
}
