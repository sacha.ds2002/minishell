/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/01 15:42:18 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/02 15:09:12 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_count_strs(char const *s, char c)
{
	int		nb;

	nb = 0;
	while (*s && *s == c)
		s++;
	while (*s)
	{
		nb++;
		while (*s && *s != c)
			s++;
		while (*s && *s == c)
			s++;
	}
	return (nb);
}

static int	ft_strlen_c(char const *s, char c)
{
	int	i;

	i = 0;
	while (s[i] && s[i] != c)
		i++;
	return (i);
}

static void	ft_free_arr(char **s, int i)
{
	while (i--)
		free(s[i]);
	free(s);
}

char	**ft_split(char const *s, char c)
{
	char	**result;
	size_t	count;
	size_t	i;

	count = ft_count_strs(s, c);
	result = (char **)ft_calloc(sizeof(char *), (count + 1));
	if (!result)
		return (0);
	i = 0;
	while (i < count)
	{
		while (*s && *s == c)
			s++;
		result[i] = ft_strndup(s, ft_strlen_c(s, c));
		if (!result[i])
		{
			ft_free_arr(result, i - 1);
			return (0);
		}
		s += ft_strlen_c(s, c);
		i++;
	}
	return (result);
}
