/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/28 16:29:23 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/02 15:03:56 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	void	*v;
	size_t	i;

	i = 0;
	v = (void *)malloc(count * size);
	if (!v)
		return (0);
	ft_memset(v, 0, count * size);
	return (v);
}
