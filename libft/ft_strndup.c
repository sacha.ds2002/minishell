/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/24 15:05:41 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/02 15:00:41 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *s1, int size)
{
	char	*str;
	int		i;

	i = 0;
	str = (char *)ft_calloc((size + 1), sizeof(char));
	if (!str)
	{
		free(str);
		return (0);
	}
	ft_strlcpy(str, s1, size + 1);
	if (size + 1)
	{
		while (i < size + 1 - 1 && s1[i])
		{
			str[i] = s1[i];
			i++;
		}
		str[i] = 0;
	}
	return (str);
}
