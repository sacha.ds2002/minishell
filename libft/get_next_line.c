/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/29 14:01:47 by sada-sil          #+#    #+#             */
/*   Updated: 2023/09/13 14:33:57 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_strchr_i(const char *s, int c)
{
	size_t	len;
	size_t	i;

	if (!s)
		return (-2);
	i = 0;
	len = ft_strlen(s);
	while (i < len && s[i] != (unsigned char)c)
		i++;
	if (s[i] == (unsigned char)c)
		return (i);
	return (-1);
}

static void	free_buffer(char **buffer, int fd)
{
	if (buffer[fd])
	{
		free(buffer[fd]);
		buffer[fd] = NULL;
	}
}

static int	read_until_eol(int fd, char **buffer)
{
	char	*tmp;
	ssize_t	bytes;

	bytes = 0;
	while (ft_strchr_i(buffer[fd], '\n') < 0)
	{
		tmp = (char *)ft_calloc(sizeof(char), BUFFER_SIZE + 1);
		bytes = read(fd, tmp, BUFFER_SIZE);
		if (bytes == -1 || !tmp)
		{
			free(tmp);
			return (0);
		}
		else if (bytes == 0)
		{
			free(tmp);
			break ;
		}
		else if (!buffer[fd])
			buffer[fd] = ft_strndup(tmp, bytes);
		else
			buffer[fd] = gnl_strjoin(buffer[fd], tmp);
		free(tmp);
	}
	return (1);
}

static char	*read_buffer(char **buffer, int fd)
{
	char	*tmp;
	char	*line;
	int		eol;

	eol = ft_strchr_i(buffer[fd], '\n');
	if (eol == -1)
	{
		line = ft_strndup(buffer[fd], (int)ft_strlen(buffer[fd]));
		free_buffer(buffer, fd);
	}
	else if (eol == -2)
		return (NULL);
	else
	{
		line = ft_strndup(buffer[fd], eol + 1);
		if (eol < (int)ft_strlen(buffer[fd]) - 1)
		{
			tmp = buffer[fd];
			buffer[fd] = ft_strndup(tmp + eol + 1, ft_strlen(tmp + eol));
			free(tmp);
		}
		else
			free_buffer(buffer, fd);
	}
	return (line);
}

char	*get_next_line(int fd)
{
	static char		*buffer[4096];
	char			*line;

	if (fd < 0 || fd > 1024 || BUFFER_SIZE < 1 || read(fd, 0, 0) < 0)
	{
		free_buffer(buffer, fd);
		return (NULL);
	}
	if (!read_until_eol(fd, buffer))
	{
		free_buffer(buffer, fd);
		return (NULL);
	}
	line = read_buffer(buffer, fd);
	if (!line || !line[0])
		return (NULL);
	return (line);
}
