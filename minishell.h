/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/29 13:35:51 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/03 17:18:21 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# define DEFAULT_INPUT "STDIN"
# define DEFAULT_OUTPUT "STDOUT"
# define LAST_PID 2147483647
# define READLINE_SIZE 4096
# define ARRAY_BUFFER 2048
# define LINESIZE 1024
# define ARGVMAX 100

# define RESET_COLOR	"\033[0m"
# define RED			"\033[0;31m"
# define GREEN			"\033[0;32m"
# define YELLOW			"\033[0;33m"
# define BLUE			"\033[0;34m"
# define MAGENTA		"\033[0;35m"
# define CYAN			"\033[0;36m"
# define WHITE			"\033[0;37m"

# include <stdio.h>
# include <errno.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <stdarg.h>
# include <limits.h>
# include <fcntl.h>
# include <signal.h>
# include <termios.h>
# include <stdbool.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <sys/syslimits.h>
# include <readline/readline.h>
# include <readline/history.h>
# include "libft/libft.h"

int	g_status;

typedef struct s_word
{
	char	*str;
	int		squote;
	void	*next;
}	t_word;

typedef struct s_pids
{
	pid_t	pid;
	void	*next;
}	t_pids;

typedef struct s_env
{
	char			*str;
	struct s_env	*next;
}				t_env;

typedef struct s_param
{
	char	*str;
	void	*next;
}	t_param;

typedef struct s_token
{
	int		id;
	int		end[2];
	char	*input;
	char	*output;
	char	*file;
	t_param	*param;
	char	*delim;
	int		append;
	void	*next;
}	t_token;

//others
char	*init_line(void);
void	signal_init(void (*signal_handler)(int));
void	prompt_handler(int sig);
void	exec_handler(int sig);
void	free_char_array(char ***str);

/*--- PARSING ---*/
t_token	*parsing(char *ol, t_env *env);
int		ft_get_type(char *str);

//tokens
t_token	*ft_get_tokens(t_word *word);
t_token	*ft_lstlast_token(t_token *lst);
t_word	*ft_set_input_output(t_token **token, t_word *word, int type);
void	ft_clear_tokens(t_token **token);
void	ft_token_addback(t_token **lst, t_token *new);
char	*ft_create_heredoc(char	*delim);

//params
t_param	*ft_create_param(char *str);
t_param	*ft_lstlast_param(t_param *lst);
void	ft_param_addback(t_param **lst, t_param *new);
void	ft_clear_params(t_param **param);

//words
t_word	*ft_split_quotes(char *s, char c);
t_word	*ft_create_word(char const *str, int length, int sep);
t_word	*ft_lstlast_word(t_word *lst);
void	ft_word_addback(t_word **lst, t_word *new);
void	ft_clear_words(t_word **word);

//env variables
void	ft_get_envariables(t_word *word, t_env *env);
int		ft_strlen_c(char *str, char c);
int		ft_tot_len(char *str, int var_len);
int		ft_len_after(char *str);
int		ft_len_var(char *str);
char	*ft_get_env_var(char *name, int len, t_env *env);
char	**ft_get_paths(char *env);

/*--- EXECUTION ---*/
void	execution(t_token *token, char **paths, t_env **env);
void	ft_child_process(t_token *token, char **paths, t_env *env);
t_env	*ft_convert_env(char **env);
char	**ft_env_to_string(t_env *env);
void	ft_env_addback(t_env **lst, t_env *new);
t_env	*ft_create_env_var(char	*str);
int		ft_count_env_var(t_env *env);
int		ft_is_builtin(char *file);

//redirections
void	redirections(t_token *token, int default_stdout);
void	close_pipes(int end[2]);
void	exit_on_failure(char *str);
int		ft_strcmp(const char *s1, const char *s2);

//BUILTINS
int		ft_cd(t_param *param, t_env *env);
int		ft_echo(t_param *param);
int		ft_env(t_env *env);
void	ft_exit(t_param *param);
int		ft_export(t_param *param, t_env **env);
int		ft_pwd(void);
int		ft_unset(t_param *param, t_env **env);

#endif